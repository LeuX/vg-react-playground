import React, {FC} from 'react';
import {LoremIpsum} from "./LoremIpsum";
import {Grid, H2, Paragraph} from "./Components";

export const ContentB: FC = () =>
    <>
        <H2>Content B</H2>
        <Grid gridTemplateColumns={"repeat(3, 1fr)"} gridGap={16}>
            <Paragraph>
                <LoremIpsum words={50}/>
            </Paragraph>
            <Paragraph>
                <LoremIpsum words={30}/>
            </Paragraph>
            <Paragraph>
                <LoremIpsum words={70}/>
            </Paragraph>
        </Grid>
    </>
