import React, {FC} from 'react';
import {Box, Grid, H2, Img, Paragraph} from "./Components";
import {LoremIpsum} from "./LoremIpsum";

export const ContentA: FC = () =>
    <>
        <H2>Content A</H2>
        <Grid gridTemplateColumns="auto 1fr" gridGap={16}>
            <Img src="https://placekitten.com/250/350"/>
            <Box>
                <Paragraph>
                    <LoremIpsum words={55}/>
                </Paragraph>
                <Paragraph>
                    <LoremIpsum words={25}/>
                </Paragraph>
                <Paragraph>
                    <LoremIpsum words={85}/>
                </Paragraph>
                <Paragraph>
                    <LoremIpsum words={45}/>
                </Paragraph>
            </Box>
        </Grid>
    </>
