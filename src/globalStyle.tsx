import {createGlobalStyle} from "styled-components";
import {PlaygroundTheme, theme} from "./theme";

export const GlobalStyle = createGlobalStyle<{ theme: PlaygroundTheme }>`
    html, body{
        padding: 0;
        margin: 0;
    }
    html{
        height: 100%;
    }
    body, div#root{
        min-height: 100%;
        height: 100%;
    }
    ${_ => theme.fonts?.faces?.join("\n")}
`
