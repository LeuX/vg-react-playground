import styled from "styled-components";
import {grid, GridProps, typography, color, space} from "styled-system";

export const Box = styled.div`
    ${color}
    ${space}
`

export const Container = styled(Box).attrs({
    p: 16,
})`
    background: linear-gradient(0deg, rgba(100,100,100,1) 0%, rgba(80,80,80,1) 100%); 
    color: white;
    min-height: 100%;
    height: 100%;
`

export const Grid = styled(Box)<GridProps>`
  display: grid;
  ${grid}
`

export const Img = styled.img``

export const Text = styled.span`
  ${typography}
  ${space}
`

export const Paragraph = styled(Text).attrs({
    fontFamily: 'paragraph',
    textAlign: 'justify',
    as: 'p',
    mt: 0,
})`
`

export const H1 = styled(Text).attrs({
    as: 'h1',
    fontFamily: 'heading',
    mt: 0,
})`
`

export const H2 = styled(Text).attrs({
    as: 'h2',
    fontFamily: 'heading',
    mt: 0,
})`
`
