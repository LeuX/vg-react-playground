import React, {FC} from "react";
import {H1} from "./Components";
import {ContentA} from "./ContentA";
import {ContentB} from "./ContentB";

export const PageContent: FC = () =>
    <>
        <H1>Hello World!</H1>
        <ContentA/>
        <ContentB/>
    </>
