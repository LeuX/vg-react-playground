import React, {FC} from 'react';
import {ThemeProvider} from "styled-components";
import {Container} from "./Components";
import {GlobalStyle} from "./globalStyle";
import {theme} from "./theme";
import {PageContent} from "./PageContent";

export const App: FC = () =>
    <>
        <ThemeProvider theme={theme}>
            <Container>
                <PageContent/>
            </Container>
        </ThemeProvider>
        <GlobalStyle/>
    </>
