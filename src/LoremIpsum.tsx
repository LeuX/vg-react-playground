import React, {FC} from 'react'
import {loremIpsum} from "lorem-ipsum";

export const LoremIpsum: FC<{ words: number }> = ({words}) => <>{loremIpsum({
    count: words,
    format: "plain",
    units: "word"
})}</>
