import {Theme} from "styled-system";
import * as CSS from "csstype";
import MonserratRegularWoff2 from "./static/fonts/montserrat-v15-latin-regular.woff2";
import MonserratRegularWoff from "./static/fonts/montserrat-v15-latin-regular.woff";
import RobotoWoff2 from "./static/fonts/roboto-v20-latin-700.woff2";
import RobotoWoff from "./static/fonts/roboto-v20-latin-700.woff";

export type FontIdentifier = "paragraph" | "heading"

export type PlaygroundTheme = Theme & {
    fonts: Record<FontIdentifier, CSS.Property.FontFamily> & { faces?: Array<string> }
}

export const theme: PlaygroundTheme = {
    fonts: {
        faces: [
            `@font-face {
                font-family: 'Montserrat';
                font-style: normal;
                font-weight: 400;
                src: url('${MonserratRegularWoff2}') format('woff2'), 
                     url('${MonserratRegularWoff}') format('woff');
            }`,
            `@font-face {
                  font-family: 'Roboto';
                  font-style: normal;
                  font-weight: 700;
                  src: url('${RobotoWoff2}') format('woff2'),
                       url('${RobotoWoff}') format('woff');
            }`,
        ],
        paragraph: 'Montserrat',
        heading: 'Roboto'
    }
}
